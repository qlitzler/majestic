package com.majestic.atlantis

import android.support.wearable.activity.WearableActivity
import com.google.gson.Gson
import com.majestic.atlantis.game.GameLogic
import com.majestic.atlantis.game.GameState
import com.majestic.atlantis.game.GsonMessageHandlerGson
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import okhttp3.*


class Socket(
    activity: WearableActivity,
    gameLogic: GameLogic,
    gson: Gson
) : GsonMessageHandlerGson(activity, gameLogic, gson) {

    private var job: Job? = null
    private val client = OkHttpClient.Builder().build()
    private val request: Request = Request.Builder().url(activity.getString(R.string.web_socket_url)).build()
    private val webSocketListener = object : WebSocketListener() {

        override fun onMessage(webSocket: WebSocket, text: String) {
            val gameObject = readMessage(text)
            activity.runOnUiThread { handleGameObject(gameObject) }
        }

        override fun onOpen(webSocket: WebSocket, response: Response?) {
            if (job == null) {
                job = launch(CommonPool) {
                    while (true) {
                        val gameObject = gameLogic.gameObject
                        if (gameObject != null) {
                            val gameState = GameState(gameObject.id, System.currentTimeMillis())
                            webSocket.send(gson.toJson(gameState))
                        }
                        delay(10000L)
                    }
                }
            }
            println("WebSocket: onOpen() -> successful $response")
        }

        override fun onClosing(webSocket: WebSocket, code: Int, reason: String?) {
            webSocket.close(1000, null)
            println("WebSocket: onClosing($code $reason)")
        }

        override fun onFailure(webSocket: WebSocket?, t: Throwable?, response: Response?) {
            println("Log WebSocket: onFailure(${t?.message})")
            job?.cancel()
            job = null
            client.newWebSocket(request, this)
        }

        override fun onClosed(webSocket: WebSocket?, code: Int, reason: String?) {
            println("WebSocket: onClosed($code $reason)")
            job?.cancel()
            job = null
            client.newWebSocket(request, this)
        }
    }
    private val webSocket = client.newWebSocket(request, webSocketListener)
}