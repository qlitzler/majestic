package com.majestic.atlantis.extension

import com.google.gson.Gson


inline fun <reified T> Gson.fromJson(string: String): T {
    return fromJson<T>(string, T::class.java)
}