package com.majestic.atlantis.game


interface GameLogic {

    var previousHint: GameObjectHint?
    var gameObject: GameObject?

    fun showTimer(gameObject: GameObjectTimer)
    fun resetGame(gameObject: GameObjectReset)
    fun showHint(gameObject: GameObjectHint, fromServer: Boolean)
    fun showAdmin(gameObject: GameObjectAdmin)
}