package com.majestic.atlantis.game


interface GameMessageHandler {

    val gameLogic: GameLogic

    fun readMessage(message: String): GameObject?
    fun handleGameObject(gameObject: GameObject?)
}