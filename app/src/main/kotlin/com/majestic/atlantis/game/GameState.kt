package com.majestic.atlantis.game

import com.google.gson.annotations.SerializedName


data class GameState(
    @SerializedName("id") val id: String,
    @SerializedName("currentTimeMillis") val currentTimeMillis: Long
)