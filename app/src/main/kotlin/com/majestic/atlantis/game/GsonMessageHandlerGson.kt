package com.majestic.atlantis.game

import android.support.wearable.activity.WearableActivity
import com.google.gson.Gson
import com.majestic.atlantis.extension.fromJson


abstract class GsonMessageHandlerGson(
    val activity: WearableActivity,
    override val gameLogic: GameLogic,
    val gson: Gson
) : GameMessageHandler {

    override fun readMessage(message: String): GameObject? {
        return when {
            message.contains("hint") -> gson.fromJson<GameObjectHint>(message)
            message.contains("timer") -> gson.fromJson<GameObjectTimer>(message)
            message.contains("reset") -> gson.fromJson<GameObjectReset>(message)
            message.contains("admin") -> gson.fromJson<GameObjectAdmin>(message)
            else -> null
        }
    }

    override fun handleGameObject(gameObject: GameObject?) {
        if (gameObject != null) {
            println("[${gameObject.id}] GameObject handled.")
            gameLogic.gameObject = gameObject
            when (gameObject) {
                is GameObjectHint -> gameLogic.showHint(gameObject, true)
                is GameObjectReset -> gameLogic.resetGame(gameObject)
                is GameObjectTimer -> gameLogic.showTimer(gameObject)
                is GameObjectAdmin -> gameLogic.showAdmin(gameObject)
            }
        } else println("Malformed json: GameObject is null.")
    }
}