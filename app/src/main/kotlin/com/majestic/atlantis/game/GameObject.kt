package com.majestic.atlantis.game

import com.google.gson.annotations.SerializedName


sealed class GameObject(
    @SerializedName("id") val id: String,
    @SerializedName("type") val type: String,
    @SerializedName("remoteCreationMillis") val remoteCreationMillis: Long
)

class GameObjectHint(
    id: String,
    type: String,
    remoteCreationMillis: Long,
    @SerializedName("text") val text: String?,
    @SerializedName("image") val image: String?
) : GameObject(id, type, remoteCreationMillis)


class GameObjectReset(
    id: String,
    type: String,
    remoteCreationMillis: Long
) : GameObject(id, type, remoteCreationMillis)

class GameObjectTimer(
    id: String,
    type: String,
    remoteCreationMillis: Long,
    @SerializedName("running") val running: Boolean,
    @SerializedName("localExpirationMillis") val localExpirationMillis: Long
) : GameObject(id, type, remoteCreationMillis)

class GameObjectAdmin(
    id: String,
    type: String,
    remoteCreationMillis: Long
) : GameObject(id, type, remoteCreationMillis)