package com.majestic.atlantis.game

import android.content.ComponentName
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.support.wearable.activity.WearableActivity
import android.util.Base64
import android.view.ViewGroup
import com.majestic.atlantis.AdminReceiver
import com.majestic.atlantis.Preferences
import com.majestic.atlantis.component.ComponentAdmin
import com.majestic.atlantis.component.ComponentHint
import com.majestic.atlantis.component.ComponentPlaceholder
import com.majestic.atlantis.component.ComponentTimer
import org.jetbrains.anko.devicePolicyManager


class Game(
    val activity: WearableActivity,
    parent: ViewGroup,
    val preferences: Preferences
) : GameLogic,
    ComponentTimer.Action,
    ComponentHint.Action,
    ComponentAdmin.Action {

    private val timer = ComponentTimer(activity, parent, this)
    private val hint = ComponentHint(activity, parent, this)
    private val placeholder = ComponentPlaceholder(activity, parent)
    private val admin = ComponentAdmin(activity, parent, this)

    private val deviceAdmin = ComponentName(activity, AdminReceiver::class.java)
    private val dpm = activity.devicePolicyManager
    private val packageName = activity.packageName

    private val notification = activity.resources.getIdentifier("alert", "raw", packageName)
    private val mediaPlayer = MediaPlayer.create(activity, notification).also {
        var count = 0
        it.setOnCompletionListener {
            if (count < 2) {
                it.seekTo(0)
                it.start()
                count++
            } else count = 0
        }
    }

    override var previousHint: GameObjectHint? = null
    override var gameObject: GameObject? = null

    init {
        val hint = preferences.lastHint
        val timer = preferences.lastTimer
        if (hint != null) {
            previousHint = hint
        }
        if (timer != null) {
            showTimer(timer)
        } else {
            placeholder.show()
        }
    }

    override fun onClickTimer() {
        previousHint?.let {
            showHint(it, false)
        }
    }

    override fun onClickHint() {
        hint.cancelDismissTimer()
        timer.show(previousHint != null)
    }

    override fun showTimer(gameObject: GameObjectTimer) {
        hint.cancelDismissTimer()
        preferences.lastTimer = gameObject
        timer.pauseOrResume(gameObject.running)
        timer.show(previousHint != null)
        val durationInMillis = gameObject.localExpirationMillis - gameObject.remoteCreationMillis
        timer.launchTimer(durationInMillis)
    }

    override fun showHint(gameObject: GameObjectHint, fromServer: Boolean) {
        if (fromServer) mediaPlayer.start()
        hint.cancelDismissTimer()
        preferences.lastHint = gameObject
        previousHint = gameObject
        when {
            gameObject.text != null -> hint.showText(gameObject.text)
            gameObject.image != null -> {
                val bytes = Base64.decode(gameObject.image, Base64.DEFAULT)
                val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
                hint.showImage(bitmap)
            }
            else -> println("Malformed Game Hint. Text and Image are both null.")
        }
    }

    override fun resetGame(gameObject: GameObjectReset) {
        hint.cancelDismissTimer()
        timer.cancel()
        placeholder.show()
        previousHint = null
        preferences.lastTimer = null
        preferences.lastHint = null
    }

    override fun showAdmin(gameObject: GameObjectAdmin) {
        admin.showAdmin()
        if (dpm.isDeviceOwnerApp(packageName)) {
            if (dpm.isUninstallBlocked(deviceAdmin, packageName)) {
                admin.setClickToUnlock()
            } else {
                admin.setClickToLock()
            }
        }
    }

    override fun onClickUnlock() {
        admin.setClickToLock()
        activity.stopLockTask()
        dpm.setUninstallBlocked(deviceAdmin, packageName, false)
    }

    override fun onClickLock() {
        admin.setClickToUnlock()
        activity.startLockTask()
        dpm.setUninstallBlocked(deviceAdmin, packageName, true)
    }
}