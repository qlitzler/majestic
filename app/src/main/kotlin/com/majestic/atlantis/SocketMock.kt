package com.majestic.atlantis

import android.support.wearable.activity.WearableActivity
import com.google.gson.Gson
import com.majestic.atlantis.game.GameLogic
import com.majestic.atlantis.game.GsonMessageHandlerGson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch


class SocketMock(
    activity: WearableActivity,
    gameLogic: GameLogic,
    gson: Gson
) : GsonMessageHandlerGson(activity, gameLogic, gson) {

    init {
        launch(UI) {
            var timer = 0L
            while (true) {
                handleEvent(timer)
                delay(1000L)
                timer += 1000L
            }
        }
        launch(UI) {
            while (true) {
                var timer = 0L
                delay(10000L)
                timer += 10000L
                gameLogic.gameObject?.let {
                    println("GameState ${timer / 1000}s: ${it.id}, ${System.currentTimeMillis()}")
                }
            }
        }
    }

    private fun handleEvent(timer: Long) {
        val message = when (timer / 1000L) {
            0L -> readAsset("message_1_reset.json")
            5L -> readAsset("message_2_timer.json")
            10L -> readAsset("message_3_hint.json")
            15L -> readAsset("message_4_timer.json")
            20L -> readAsset("message_5_timer.json")
            25L -> readAsset("message_6_hint.json")
            90L -> readAsset("message_7_reset.json")
            100L -> readAsset("message_8_admin.json")
            else -> null
        }

        if (message != null) {
            val gameObject = readMessage(message)
            handleGameObject(gameObject)
        }
    }

    private fun readAsset(fileName: String): String {
        return activity.assets.open(fileName).bufferedReader().readText()
    }
}