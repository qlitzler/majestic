package com.majestic.atlantis.component

import android.graphics.drawable.AnimationDrawable
import android.support.wearable.activity.WearableActivity
import android.view.ViewGroup
import com.majestic.atlantis.R
import com.majestic.atlantis.extension.inflate
import kotlinx.android.synthetic.main.placeholder.view.*
import org.jetbrains.anko.backgroundDrawable


class ComponentPlaceholder(
    override val activity: WearableActivity,
    override val parent: ViewGroup
) : Component {

    private val view = parent.inflate(R.layout.placeholder)

    fun show() {
        parent.removeAllViews()
        parent.addView(view)
        (view.placeHolderPixels.backgroundDrawable as AnimationDrawable).start()
    }
}