package com.majestic.atlantis.component

import android.graphics.Bitmap
import android.support.wearable.activity.WearableActivity
import android.text.method.ScrollingMovementMethod
import android.view.ViewGroup
import com.majestic.atlantis.R
import com.majestic.atlantis.extension.inflate
import kotlinx.android.synthetic.main.hint_image.view.*
import kotlinx.android.synthetic.main.hint_text.view.*
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.imageBitmap


class ComponentHint(
    override val activity: WearableActivity,
    override val parent: ViewGroup,
    val action: Action
) : Component {

    interface Action {

        fun onClickHint()
    }

    private var timerDismiss: Job? = null
    private val viewHintText = parent.inflate(R.layout.hint_text)
    private val viewHintImage = parent.inflate(R.layout.hint_image)

    fun showText(text: String) {
        parent.removeAllViews()
        parent.addView(viewHintText)
        viewHintText.hintText.text = text
        viewHintText.hintText.movementMethod = ScrollingMovementMethod()
        viewHintText.setOnClickListener { action.onClickHint() }
        startDismissTimer()
    }

    fun showImage(bitmap: Bitmap) {
        parent.removeAllViews()
        parent.addView(viewHintImage)
        viewHintImage.hintImage.imageBitmap = bitmap
        viewHintImage.setOnClickListener { action.onClickHint() }
        startDismissTimer()
    }

    fun cancelDismissTimer() {
        timerDismiss?.cancel()
        timerDismiss = null
    }

    fun startDismissTimer() {
        if (timerDismiss == null) {
            timerDismiss = launch(UI) {
                delay(45000L)
                action.onClickHint()
            }
            timerDismiss = null
        }
    }
}