package com.majestic.atlantis.component

import android.graphics.drawable.AnimationDrawable
import android.support.wearable.activity.WearableActivity
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import com.majestic.atlantis.R
import com.majestic.atlantis.extension.inflate
import kotlinx.android.synthetic.main.timer.view.*
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.android.UI
import org.jetbrains.anko.backgroundDrawable
import java.text.SimpleDateFormat
import java.util.*


class ComponentTimer(
    override val activity: WearableActivity,
    override val parent: ViewGroup,
    val action: ComponentTimer.Action
) : Component {

    interface Action {

        fun onClickTimer()
    }

    private var timer: Job? = null
    private var running: Boolean = false

    private val view = parent.inflate(R.layout.timer).also {
        it.setOnClickListener { action.onClickTimer() }
    }

    private val date = SimpleDateFormat("mm:ss", Locale.getDefault())

    fun pauseOrResume(running: Boolean) {
        this.running = running
    }

    fun launchTimer(duration: Long) {
        timer?.cancel()
        timer = launch(CommonPool) {
            var timer = duration
            while (timer >= 0) {
                run(UI) { view.timerDuration.text = date.format(Date(timer)) }
                while (running == false);
                delay(1000L)
                timer -= 1000L
            }
            this@ComponentTimer.timer = null
        }

    }

    fun cancel() {
        timer?.cancel()
        timer = null
    }

    fun show(isPreviousHintVisible: Boolean) {
        if (parent.getChildAt(0) != view) {
            parent.removeAllViews()
            parent.addView(view)
            rotation(view.circleSmall, false, 1500L)
            rotation(view.circleLarge, true, 4000L)
        }
        (view.timerPixels.backgroundDrawable as AnimationDrawable).start()
        view.previousHint.visibility = if (isPreviousHintVisible) View.VISIBLE else View.INVISIBLE
    }

    fun rotation(view: View, inverse: Boolean, duration: Long) {
        val start = if (inverse) 360f else 0f
        val end = if (inverse) 0f else 360f
        val animation = RotateAnimation(start, end, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f).also {
            it.repeatCount = Animation.INFINITE
            it.duration = duration
            it.interpolator = LinearInterpolator()
        }
        view.startAnimation(animation)
    }
}