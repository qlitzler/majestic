package com.majestic.atlantis.component

import android.support.wearable.activity.WearableActivity
import android.view.ViewGroup
import com.majestic.atlantis.R
import com.majestic.atlantis.extension.inflate
import kotlinx.android.synthetic.main.admin.view.*


class ComponentAdmin(
    override val activity: WearableActivity,
    override val parent: ViewGroup,
    val action: Action
) : Component {

    interface Action {

        fun onClickUnlock()
        fun onClickLock()
    }

    private val view = parent.inflate(R.layout.admin)

    fun showAdmin() {
        parent.removeAllViews()
        parent.addView(view)
    }

    fun setClickToUnlock() {
        view.adminLock.text = activity.getString(R.string.click_to_unlock)
        view.adminLock.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock_closed, 0, 0, 0)
        view.adminLock.setOnClickListener { action.onClickUnlock() }
    }

    fun setClickToLock() {
        view.adminLock.text = activity.getString(R.string.click_to_lock)
        view.adminLock.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock_open, 0, 0, 0)
        view.adminLock.setOnClickListener { action.onClickLock() }
    }
}