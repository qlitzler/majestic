package com.majestic.atlantis.component

import android.support.wearable.activity.WearableActivity
import android.view.ViewGroup


interface Component {

    val activity: WearableActivity
    val parent: ViewGroup
}