package com.majestic.atlantis

import android.content.ComponentName
import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import com.google.gson.Gson
import com.majestic.atlantis.game.Game
import kotlinx.android.synthetic.main.activity.*
import org.jetbrains.anko.devicePolicyManager


class GameActivity : WearableActivity() {

    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)

        val deviceAdmin = ComponentName(this, AdminReceiver::class.java)
        if (devicePolicyManager.isDeviceOwnerApp(packageName)) {
            devicePolicyManager.setLockTaskPackages(deviceAdmin, arrayOf(packageName))
            if (devicePolicyManager.isUninstallBlocked(deviceAdmin, packageName)) {
                startLockTask()
            }
        }

        val preferences = Preferences(this, gson)

        val game = Game(this, container, preferences)
//        SocketMock(this, game, gson)
        Socket(this, game, gson)
    }
}