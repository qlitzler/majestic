package com.majestic.atlantis

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.majestic.atlantis.game.GameObjectHint
import com.majestic.atlantis.game.GameObjectTimer


class Preferences(context: Context, private val gson: Gson) {

    private enum class Keys {
        LastTimer,
        LastHint
    }

    private val preferences: SharedPreferences = context.getSharedPreferences("Atlantis", Context.MODE_PRIVATE)

    private fun SharedPreferences.remove(key: Keys) = edit().remove(key.name).apply()
    private fun SharedPreferences.getString(key: Keys, defaultValue: String? = null): String? = getString(key.name, defaultValue)
    private fun SharedPreferences.putString(key: Keys, value: String?) = edit().putString(key.name, value).apply()
    private fun SharedPreferences.getBoolean(key: Keys): Boolean = getBoolean(key.name, true)
    private fun SharedPreferences.putBoolean(key: Keys, value: Boolean) = edit().putBoolean(key.name, value).apply()
    private fun SharedPreferences.getInt(key: Keys, defaultValue: Int = 0): Int = getInt(key.name, defaultValue)
    private fun SharedPreferences.putInt(key: Keys, value: Int) = edit().putInt(key.name, value).apply()

    private fun setValue(key: Keys, value: Any?) {
        if (value == null) {
            preferences.remove(key)
        } else {
            when (value) {
                is String -> preferences.putString(key, value)
                is Boolean -> preferences.putBoolean(key, value)
                is Int -> preferences.putInt(key, value)
                else -> preferences.putString(key, gson.toJson(value))
            }
        }
    }

    private inline fun <reified T> getValue(key: Keys): T? {
        return try {
            gson.fromJson(preferences.getString(key), T::class.java)
        } catch (exception: Exception) {
            preferences.putString(key, null)
            null
        }
    }

    var lastTimer: GameObjectTimer?
        get() = getValue<GameObjectTimer>(Keys.LastTimer)
        set(value) = setValue(Keys.LastTimer, value)

    var lastHint: GameObjectHint?
        get() = getValue<GameObjectHint>(Keys.LastHint)
        set(value) = setValue(Keys.LastHint, value)
}